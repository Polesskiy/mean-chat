#Simple nodejs chat on MEAN stack.

    Allow users to register in and chat with each other.

Uses Node.js, MongoDB (mongoose) on mLab, Express, Angular. For deploying on Heroku.

Test admin access:

    login: admin
    pass: admin

Test user access:

    login: user
    pass: user